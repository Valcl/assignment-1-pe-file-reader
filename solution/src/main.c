/// @file 
/// @brief Main application file

#define _CRT_SECURE_NO_WARNINGS
#include "headers.h"
#include <string.h> 
#define MAIN_OFFSET 0x3c


/**
 * @brief Stores PE file data
*/
#if defined _MSC_VER
#pragma pack(push, 1)
#endif
struct
#if defined __clang__ || __GNUC__
    __attribute__((packed))
#endif
    PEFile
{
  /// @name File headers
  ///@{
  
  /// File magic
  uint32_t magic;
  /// Main header
  struct PEHeader header;
  /// Array of section headers with the size of header.number_of_sections
  struct SectionHeader *section_headers;
  ///@}

};
#if defined _MSC_VER
#pragma pack(pop)
#endif


/// @brief Print usage test
/// @param[in] f File to print to (e.g., stdout)
void usage(FILE* f)
{
	fprintf(f, "Usage:  <in_file> <section_name> <out_file>\n");
}

/// @brief Application entry point
/// @param[in] argc Number of command line arguments
/// @param[in] argv Command line arguments
/// @return 0 in case of success or error code
int main(int argc, char** argv)
{
    if (argc != 4)
  	{
    	printf("Usage: %s <source-pe-file> <section-name> <output-bin-image>\n", argv[0]);
        return 1;
    }
	
    usage(stdout);

    FILE* in, * out;

    in = fopen(argv[1], "rb");
    out = fopen(argv[3], "wb");

    if (!in) {
        printf("Couldn't open input file\n");
    	return 1;
    }
	
	if (!out) {
		printf("Couldn't open file\n");
		return 1;
	}

	struct PEFile *pe_file = malloc(sizeof(struct PEFile));

    fseek(in, MAIN_OFFSET, SEEK_SET);
    
    uint32_t effective_offset;
    fread(&effective_offset, sizeof(effective_offset), 1, in);
    fseek(in, (long) effective_offset, SEEK_SET);  

    if (fread(&pe_file->magic, sizeof(pe_file->magic), 1, in) != 1) {
        printf("Couldn't read magic\n");
        return 1;
    }
    if (fread(&pe_file->header, sizeof(pe_file->header), 1, in) != 1) {
        printf("Couldn't read header\n");
        return 1;
    }
    pe_file->section_headers = malloc(pe_file->header.NumberOfSections * sizeof(struct SectionHeader));

    fseek(in, pe_file->header.SizeOfOptionalHeader, SEEK_CUR);

    if (fread(pe_file->section_headers, sizeof(struct SectionHeader), pe_file->header.NumberOfSections, in) != 1) {
        printf("Couldn't read section header\n");
    }


    for (size_t i = 0; i < pe_file->header.NumberOfSections; i++){
        if ( strcmp((char*)pe_file->section_headers[i].Name, argv[2]) == 0) {

            fseek(in, pe_file->section_headers[i].PointerToRawData, SEEK_SET);

            char *raw_data = malloc(pe_file->section_headers[i].SizeOfRawData);
            fread(raw_data, pe_file->section_headers[i].SizeOfRawData, 1, in);
            fwrite(raw_data, pe_file->section_headers[i].SizeOfRawData, 1, out);
            free(raw_data);

        }
        
    }

	free(pe_file->section_headers);
  	free(pe_file);
  	fclose(in);
  	fclose(out);

  	return 0;
}
