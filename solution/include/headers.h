/// @file 
/// @brief Contains PE structures and functions definitions


#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

/// @brief MS-DOS offset
#define MAIN_OFFSET 0x3c


/**
 * @brief Stores PE header data
*/
#if defined _MSC_VER
#pragma pack(push, 1)
#endif
struct
#if defined __clang__ || __GNUC__
    __attribute__((packed))
#endif
    PEHeader
{
    uint16_t Machine;
    uint16_t NumberOfSections;
    uint32_t TimeDateStamp;
    uint32_t PointerToSymbolTable;
    uint32_t NumberOfSymbols;
    uint16_t SizeOfOptionalHeader;
    uint16_t Characteristics;
};
#if defined _MSC_VER
#pragma pack(pop)
#endif


/**
 * @brief Stores section header data
*/
#if defined _MSC_VER
#pragma pack(push, 1)
#endif
struct
#if defined __clang__ || __GNUC__
    __attribute__((packed))
#endif
    SectionHeader
{
    uint8_t Name[8];
    uint32_t VirtualSize;
    uint32_t VirtualAddress;
    uint32_t SizeOfRawData;
    uint32_t PointerToRawData;
    uint32_t PointerToRelocations;
    uint32_t PointerToLinenumbers;
    uint16_t NumberOfRelocations;
    uint16_t NumberOfLinenumbers;
    uint32_t Characteristics;
};
#if defined _MSC_VER
#pragma pack(pop)
#endif

